#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "model/TTModel.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("treeModel", TreeModel::getInstance(qobject_cast<QObject *>(&app)));

    QStringList vars;

    TreeModel::getInstance()->addItem("GrandChild 1", "Parent 1/Child 1");

    TreeModel::getInstance()->addItem("GrandChild 2", "Parent 1/Child 1");

    TreeModel::getInstance()->addItem("Child 2", "Parent 1");

    TreeModel::getInstance()->addItem("Parent 2");

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
