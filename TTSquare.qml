import QtQuick 2.0

Item {
    width: 70

    property color colorName: "lightblue"

    Row {
        Rectangle {
            width: 35
            height: 35

            Rectangle {
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                width: 20
                height: 5
                color: colorName
            }
        }

        Rectangle {
            width: 35
            height: 35

            Rectangle {
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                width: 20
                height: 20
                color: "lightblue"
            }
        }
    }
}
