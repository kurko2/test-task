#ifndef TREEMODEL_H
#define TREEMODEL_H

#include <QAbstractListModel>
#include <QStringList>
#include <QSharedPointer>
#include "qttricks/qqmlhelpers.h"

class TreeModelItem;

class TreeModel : public QAbstractListModel
{
    Q_OBJECT
    SINGLETON(TreeModel)

public:
    enum FixedRoles {
        LabelRole = Qt::UserRole + 1,
        ItemsCountRole,
        HasChildrenRole,
        ChildrenModel,
        FixedRolesEnd
    };

    TreeModel(QObject *parent = 0);
    ~TreeModel();
    void clear();
    void addItem(QString label, QString path = QString());

    Q_INVOKABLE int rowCount(const QModelIndex & parent = QModelIndex()) const;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

protected:
    QHash<int, QByteArray> roleNames() const;
    QList<QSharedPointer<TreeModelItem> > m_items;
    QMap<QString, QSharedPointer<TreeModelItem> > m_itemsPathMap;
};

#endif // TREEMODEL_H
