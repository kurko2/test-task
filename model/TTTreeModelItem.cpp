#include <QDebug>

#include "TTTreeModelItem.h"
#include "TTModel.h"

TreeModelItem::TreeModelItem(QString label)
{
    m_label = label;
}

TreeModelItem::~TreeModelItem()
{
}

QString TreeModelItem::label() const
{
    return m_label;
}

void TreeModelItem::setLabel(QString label)
{
    m_label = label;
}

void TreeModelItem::setData(QStringList data)
{
    m_data = data;
}

QVariant TreeModelItem::data(int index)
{
    if (index < 0 || index >= m_data.count())
        return QVariant();

    return m_data.at(index);
}

void TreeModelItem::addChild(QString label, QString path)
{
    if (m_children.isNull()) {
        m_children.reset(new TreeModel());
    }
    m_children->addItem(label, path);
}

bool TreeModelItem::hasChildren()
{
    if (m_children != NULL)
        return true;

    return false;
}

TreeModel *TreeModelItem::children()
{
    return m_children.data();
}


