#ifndef TREEMODELITEM_H
#define TREEMODELITEM_H

#include <QStringList>
#include <QScopedPointer>

class TreeModel;

class TreeModelItem
{
public:
    TreeModelItem(QString label);
    ~TreeModelItem();

    QString label() const;
    void setLabel(QString label);

    void setData(QStringList data);

    QVariant data(int index);


    void addChild(QString label, QString path = QString());

    bool hasChildren();

    TreeModel *children();

private:
    QString m_label;
    QStringList m_data;
    QScopedPointer<TreeModel> m_children;
};

#endif // TREEMODELITEM_H
