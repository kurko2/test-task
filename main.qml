import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12

Window {
    id: mainWindow

    visible: true
    width: 960
    height: 480
    title: qsTr("Test Task")

    ListView {
        id: treeModelView

        anchors.left: parent.left
        boundsBehavior: Flickable.StopAtBounds
        width: 350
        height: 600
        model: treeModel
        delegate: Component {
            Loader {
                width: parent.width
                source: hasChildren ? "TTNode.qml" : "TTItem.qml"
                onLoaded: {
                    item.textLabel = label
                    if (hasChildren) {
                        item.nodeChildrenModel = childrenModel
                        item.childrenHeight = (childrenModel.rowCount() * 35)
                    }
                }
            }
        }
    }

    ListView {
        id: listView

        anchors.right: parent.right
        width: 350
        height: 600
        model: treeModel
        delegate: Text {
            text: label
            font.pixelSize: 25
        }
    }
}

