import QtQuick 2.0

Rectangle {
    id: nodeContainer
    height: nodeLabel.height + nodeChildrenView.height

    property string textLabel
    property var nodeChildrenModel
    property bool isExpanded: false
    property int childrenHeight: 0
    property int variableHeight: 0

    signal toggled(bool expanded, int newHeight)

    Row {
        height: parent.height
        width: parent.width

        TTSquare {
            height: parent.height
            colorName: "lightblue"
        }

        Text {
            id: nodeLabel
            width: parent.width
            height: 35
            text: textLabel
            verticalAlignment: Text.AlignVCenter
        }
    }

    MouseArea {
        width: parent.width
        height: 35
        onClicked: {
            isExpanded = !isExpanded
            nodeContainer.toggled(isExpanded, childrenHeight)
        }
    }

    Rectangle {
        height: 1
        width: parent.width
        y: 34
        color: "gray"
    }

    ListView {
        id: nodeChildrenView
        visible: isExpanded
        width: parent.width
        x: 35
        y: nodeLabel.height
        height: isExpanded ? (childrenHeight + variableHeight) : 0
        model: nodeChildrenModel
        boundsBehavior: Flickable.StopAtBounds
        delegate:
            Component {
                Loader {
                    width: parent.width // 350
                    //height: 35
                    source: hasChildren ? "TTNode.qml" : "TTItem.qml"
                    onLoaded: {
                        item.textLabel = label
                        if (hasChildren) {
                            item.nodeChildrenModel = childrenModel
                            item.childrenHeight = (childrenModel.rowCount() * 35)
                        }
                    }

                    Connections {
                         target: item
                         onToggled: (item.isExpanded) ? (variableHeight += item.childrenHeight) : (variableHeight -= item.childrenHeight)
                    }
                }
        }
    }
}

