#include "TTModel.h"
#include "TTTreeModelItem.h"

TreeModel::TreeModel(QObject *parent)
    : QAbstractListModel(parent)
{

}

TreeModel::~TreeModel()
{
    clear();
}

void TreeModel::clear()
{
    int itemsCount = m_items.count();
    beginRemoveRows(QModelIndex(), 0, itemsCount - 1);
    for (int i = 0; i < itemsCount; i++)
        m_items.takeLast().reset();
    m_items.clear();
    m_itemsPathMap.clear();
    endRemoveRows();
}

void TreeModel::addItem(QString label, QString path)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());

    if (path.isEmpty()) {
        QSharedPointer<TreeModelItem> item = QSharedPointer<TreeModelItem>(new TreeModelItem(label));
        m_items.append(item);
    } else {
        QSharedPointer<TreeModelItem> item;
        QStringList pathList = path.split("/");
        if (m_itemsPathMap.contains(pathList.at(0)))
            item = m_itemsPathMap[pathList.at(0)];
        else {
            item = QSharedPointer<TreeModelItem>(new TreeModelItem(pathList.at(0)));
            m_items.append(item);
            m_itemsPathMap[pathList.at(0)] = item;
        }

        if (pathList.count() == 1)
            item->addChild(label);
        else {
            QString newPath = path.mid(path.indexOf("/") + 1);
            item->addChild(label, newPath);
        }
    }

    endInsertRows();
}

int TreeModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)

    return m_items.count();
}

QVariant TreeModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() >= m_items.count())
        return QVariant();

    int itemRow = index.row();

    switch (role)
    {
    case LabelRole:
        return m_items.at(itemRow)->label();
    case ItemsCountRole:
        return m_items.count();
    case HasChildrenRole:
        return m_items.at(itemRow)->hasChildren();
    case ChildrenModel:
        return QVariant::fromValue(m_items.at(itemRow)->children());
    }

    return m_items.at(index.row())->data(role - FixedRolesEnd);
}


QHash<int, QByteArray> TreeModel::roleNames() const
{
    const QHash<int, QByteArray> roles({{LabelRole, "label"}, {ItemsCountRole, "itemsCount"}, {HasChildrenRole, "hasChildren"}, {ChildrenModel,"childrenModel"}});
    return roles;
}

