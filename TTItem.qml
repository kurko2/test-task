import QtQuick 2.0

Rectangle {
    id: leafDelegate

    height: 35
    color: "transparent"

    property string textLabel
    property string lCapital
    property string lCurrency

    signal clicked
    signal toggled(bool expanded, int newHeight)

    Row {
        height: parent.height

        TTSquare {
            height: parent.height
            colorName: "transparent"
        }

        Text {
            width: 150
            height: parent.height
            text: textLabel
            verticalAlignment: Text.AlignVCenter
        }
    }

    Rectangle {
        height: 1
        width: parent.width
        y: parent.height - 1
        color: "gray"
    }

    MouseArea {
        anchors.fill: parent
        onClicked: leafDelegate.clicked()
    }
}

